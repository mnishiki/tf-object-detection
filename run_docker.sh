# docker run as current user
docker run -v /etc/group:/etc/group:ro -v /etc/passwd:/etc/passwd:ro -u $(id -u):$(id -g) --gpus all -it --rm --name od-gpu -p 8888:8888 -v $PWD/work:/tf/work object-detection-api

# docker run as root user
# docker run  --gpus all -it --rm --name od-gpu -p 8888:8888 -v $PWD/work:/tf/work object-detection-api
