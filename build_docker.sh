# docker build -t od .
docker build -t object-detection-api .

# if you build cpu (no gpu) docker image, please run following code
# docker build -t object-detection-api-cpu -f Dockerfile.cpu