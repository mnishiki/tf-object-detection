# How to run

1. Docker ビルド
   以下のコマンドを実行し、Docker のビルドをします。

```
./build_docker.sh
```

docker exec -it tensorflow-gpu /bin/bash

2. docker コンテナの起動
   以下のコマンドを実行し、Docker のビルドをします。

```
./run_docker.sh
```

## モデル学習の準備

### データセットの準備

学習するためには学習データを TF Record 形式に変換することが必要です。
PASCAL VOC 形式のファイルを TF Record 形式に変換する場合は`work/script/generate_tf_records.py`を使います。
VOTT でアノテーションした場合は、export 時に TFRecord 形式を指定することが可能です。

export したファイルは、以下のフォルダにコピーします。

- train データ(TFRecord 形式)を`work/data/train_data`フォルダにコピー
- val データ(TFRecord 形式)を`work/data/val_data`フォルダにコピー
- test データ(TFRecord 形式)を`work/data/test_data`フォルダにコピー

### tf_label_map.pbtxt の準備

- `work/data`フォルダに tf_label_map.pbtxt を作成します。

tf_label_map.pbtxt の例としては以下です。id は 0 からではなく 1 から始めます。

```
item {
 id: 1
 name: 'blue'
}
item {
 id: 2
 name: 'enji'
}
```

最終的に、`work/data`は以下の構造となります。

-- work/data  
　　| - tf_label_map.pbtxt  
　　| - train_data/ ← train 用の TFRecord が格納されているフォルダ  
　　| - val_data/ ← val 用の TFRecord が格納されているフォルダ  
　　| - test_data/ ← test 用の TFRecord が格納されているフォルダ  
　　| - test_images/ ← （任意）test 用の画像が格納されているフォルダ

### checkpoint の準備と pipeline.config の準備

- `work/models`フォルダに以下のように作ります。

-- work/models  
　　| - パターン名  
　　　　　 | - pipeline.config  
　　　　　 | - pretrained_checkpoint ← 初期の重みファイル  
　　　　　　　 | - checkpoint  
　　　　　　　 | - ckpt-0.data-00000-of-00001  
　　　　　　　 | - ckpt-0.index  
　　　　　 | - trained_checkpoint

初期の重みファイルは[リンク](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md)よりダウンロードします。

以下は、efficientdet_d0 のモデルをダウンロードする方法です。

```
wget http://download.tensorflow.org/models/object_detection/tf2/20200711/efficientdet_d0_coco17_tpu-32.tar.gz
# extract the downloaded file
tar -xzvf efficientdet_d0_coco17_tpu-32.tar.gz
```

## モデル学習

以下のスクリプトを実行します。パラメータはスクリプトファイルを編集してください。

```
/tf/work/script/train.sh
```

## モデルの検証

テストデータで検証する場合は、以下のスクリプトを実行します。
pipeline.config で eval_input_reader を eval から test データにパスを変更して下さい。

```
/tf/work/script/eval.sh
```

## モデルの export

以下のスクリプトを実行します。パラメータはスクリプトファイルを編集してください。

```
/tf/work/script/export_model.sh
```

## export したモデルの入出力の確認

```
export SAVED_MODEL_DIR="/tf/work/models/efficientdet_d0_coco17/exported_model/saved_model"
saved_model_cli show --dir ${SAVED_MODEL_DIR} --tag_set serve --all
```

## export したモデルによる plot

以下のスクリプトを実行します。パラメータはスクリプトファイルを編集してください。

```
python /tf/work/script/plot_images_from_scrach.py
```

Framework で plot ライブラリが用意されていますが、
detection の数が多いと正しく動作しない場合があります。

```
python /tf/work/script/plot_images_by_lib.py
```

## tensorflow.js への変換

以下のスクリプトを実行します。パラメータはスクリプトファイルを編集してください。

```
/tf/work/script/export_tensorflowjs.sh
```

## tensorflow Lite への変換

### centernet の場合

以下のスクリプトを実行します。

```
/tf/work/script/export_tflite.sh
```

# FAQ

Error: TensorList shape mismatch: Shapes -1 and 3 must match #4641
https://github.com/tensorflow/tfjs/issues/4641
