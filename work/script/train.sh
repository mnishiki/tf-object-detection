#!/bin/bash

set -eux

export SCRIPT_PATH="/tf/github/models/research/object_detection"
export PIPELINE_CONFIG_PATH="/tf/work/models/efficientdet_d0_coco17/pipeline.config"
export MODEL_DIR="/tf/work/models/efficientdet_d0_coco17/trained_model"

mkdir -p ${MODEL_DIR}

python ${SCRIPT_PATH}/model_main_tf2.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=300000 \
    --checkpoint_every_n=500 \
    --alsologtostderr 2>&1 | tee ${MODEL_DIR}/train.log &

# eval on CPU
export CUDA_VISIBLE_DEVICES="-1"
export CHECKPOINT_DIR=${MODEL_DIR}

python object_detection/model_main_tf2.py \
 --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
 --model_dir=${MODEL_DIR} \
 --checkpoint_dir=${CHECKPOINT_DIR} \
 --sample_1_of_n_eval_examples=1 \ # If 1, will sample all examples.
 --alsologtostderr 2>&1 | tee ${MODEL_DIR}/eval.log
