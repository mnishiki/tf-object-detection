#!/bin/bash

set -eux

# eval on CPU
export CUDA_VISIBLE_DEVICES="-1"

export PIPELINE_CONFIG_PATH="/tf/work/models/efficientdet_d0_coco17/pipeline_for_test.config"
export MODEL_DIR="/tf/work/models/efficientdet_d0_coco17/trained_model"
export CHECKPOINT_DIR="/tf/work/models/efficientdet_d0_coco17/trained_model"

python object_detection/model_main_tf2.py \
 --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
 --model_dir=${MODEL_DIR} \
 --checkpoint_dir=${CHECKPOINT_DIR} \
 --sample_1_of_n_eval_examples=1 \ # If 1, will sample all examples.
 --alsologtostderr 2>&1 | tee ${MODEL_DIR}/test.log
