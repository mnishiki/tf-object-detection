#!/bin/bash
set -eux

export PIPELINE_CONFIG_PATH="/tf/work/models/efficientdet_d0_coco17/pipeline.config"
export CHECKPOINT_DIR="/tf/work/models/efficientdet_d0_coco17/trained_model"
export EXPORT_DIR="/tf/work/models/efficientdet_d0_coco17/exported_model"

python object_detection/exporter_main_v2.py \
    --input_type=image_tensor \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --trained_checkpoint_dir=${CHECKPOINT_DIR} \
    --output_directory=${EXPORT_DIR} 