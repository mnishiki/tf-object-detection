#!/bin/bash
set -eux

export INPUT_SAVED_MODEL_DIR="/tf/work/models/efficientdet_d0_coco17/exported_model/saved_model"
export OUTPUT_WEBMODEL_MODEL_DIR="/tf/work/models/efficientdet_d0_coco17/exported_model/tensorflowjs_model"
tensorflowjs_converter \
    --input_format=tf_saved_model \
    --saved_model_tags=serve \
    ${INPUT_SAVED_MODEL_DIR} \
    ${OUTPUT_WEBMODEL_MODEL_DIR}
