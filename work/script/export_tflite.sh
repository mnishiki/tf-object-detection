#!/bin/bash
set -eux

export SAVED_MODEL_DIR="/tf/work/models/centernet_hg104_512x512_coco17/exported_model/saved_model"
export OUTPUT_MODEL_DIR="/tf/work/models/centernet_hg104_512x512_coco17/exported_model/tflite"
export OUTPUT_NAME="model.tflite"

mkdir -p ${OUTPUT_MODEL_DIR}

export PIPELINE_CONFIG_PATH="/tf/work/models/centernet_hg104_512x512_coco17/pipeline.config"
export CHECKPOINT_DIR="/tf/work/models/centernet_hg104_512x512_coco17/trained_model"

python object_detection/export_tflite_graph_tf2.py \
  --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
  --trained_checkpoint_dir=${CHECKPOINT_DIR} \
  --output_directory=${OUTPUT_MODEL_DIR} \
  --centernet_include_keypoints=false \
  --max_detections=1000 \
  --config_override=" \
    model{ \
      center_net { \
        image_resizer { \
          fixed_shape_resizer { \
            height: 512 \
            width: 512 \
          } \
        } \
      } \
    }"


tflite_convert \
    --output_file=/tf/work/models/centernet_hg104_512x512_coco17/exported_model/tflite/model.tflite \
    --saved_model_dir=/tf/work/models/centernet_hg104_512x512_coco17/exported_model/tflite/saved_model