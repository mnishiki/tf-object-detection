#!/bin/bash
set -eux

export CHECKPOINT_DIR="~/github/tf-detection-from-scrach-2/work/models/efficientdet_d0_coco17_jr"

tensorboard --logdir ${CHECKPOINT_DIR}