import tensorflow as tf
import glob
import copy
import cv2
import numpy as np
import pathlib
import os

PATH_TO_LABELS = '/tf/work/data/tf_label_map.pbtxt'
TEST_IMAGE_DIR = '/tf/work/data/test_images'
VIDEO_PATH = '/tf/work/data/test_movie/test.mp4'
PATH_TO_SAVED_MODEL = "/tf/work/models/efficientdet_d0_coco17_jr/exported_model/saved_model"
OUTPUT_DIR = "/tf/work/models/efficientdet_d0_coco17_jr/plot_images"
SCORE_THRESHOLD=0.05

# 推論用関数(Function for inference)
def run_inference_single_image(image, inference_func):
    tensor = tf.convert_to_tensor(image)
    output = inference_func(tensor)

    output['num_detections'] = int(output['num_detections'][0])
    output['detection_classes'] = output['detection_classes'][0].numpy()
    output['detection_boxes'] = output['detection_boxes'][0].numpy()
    output['detection_scores'] = output['detection_scores'][0].numpy()
    return output


if __name__ == "__main__":

    # モデルの読込
    DEFAULT_FUNCTION_KEY = 'serving_default'
    loaded_model = tf.saved_model.load(PATH_TO_SAVED_MODEL)
    inference_func = loaded_model.signatures[DEFAULT_FUNCTION_KEY]

    image_paths = sorted(glob.glob(TEST_IMAGE_DIR + '/*.jpg'))

    cap = cv2.VideoCapture(VIDEO_PATH)

    output_path = os.path.join(OUTPUT_DIR, 'detection_'+ VIDEO_PATH.split("/")[-1])
    frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    out = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(*"mp4v"), 10, (frame_width, frame_height))


    # create folder
    pathlib.Path(OUTPUT_DIR).mkdir(parents=True, exist_ok=True)

    # 推論(Inference)
    # for image_path in image_paths:
    #     print(image_path)

    while (cap.isOpened()):
        ret, image = cap.read()
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break

        # image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
        debug_image = copy.deepcopy(image)

        image_width, image_height = image.shape[1], image.shape[0]
        image = image[:, :, [2, 1, 0]]  # BGR2RGB
        image_np_expanded = np.expand_dims(image, axis=0)

        output = run_inference_single_image(image_np_expanded, inference_func)

        num_detections = output['num_detections']
        print(f"num_detections: {num_detections}")
        for i in range(num_detections):
            score = output['detection_scores'][i]
            bbox = output['detection_boxes'][i]
            # class_id = output['detection_classes'][i].astype(np.int)

            if score < SCORE_THRESHOLD:
                continue

            x1, y1 = int(bbox[1] * image_width), int(bbox[0] * image_height)
            x2, y2 = int(bbox[3] * image_width), int(bbox[2] * image_height)

            # 推論結果描画(Inference result drawing)
            cv2.rectangle(debug_image, (x1, y1), (x2, y2), (255, 255, 255), 2)
            cv2.putText(debug_image, str('{:.2f}'.format(score)), (x1, y1-10), cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 255, 255), 2, cv2.LINE_AA)
            cv2.rectangle(debug_image, (x1, y1), (x2, y2), (255, 255, 255), 2)
        
        out.write(debug_image)

        # image_name = os.path.basename(image_path)

        # output_image_path = str(pathlib.Path(OUTPUT_DIR) / image_name)
        # print(output_image_path)

        # cv2.imwrite(output_image_path, debug_image)
    
    cap.release()
    out.release()