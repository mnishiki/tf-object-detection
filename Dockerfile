FROM tensorflow/tensorflow:2.4.1-gpu-jupyter
# FROM tensorflow/tensorflow:nightly-gpu-py3-jupyter


RUN curl -OL https://github.com/google/protobuf/releases/download/v3.2.0/protoc-3.2.0-linux-x86_64.zip \
&& unzip protoc-3.2.0-linux-x86_64.zip -d protoc3 \
&& mv protoc3/bin/* /usr/local/bin/ \
&& mv protoc3/include/* /usr/local/include/ \
&& rm -rf protoc3 protoc-3.2.0-linux-x86_64.zip

RUN mkdir -p /tf/github
WORKDIR /tf/github

# Accept EULA for MS fonts
# ttf-mscorefonts-installer is needed by visualization_utils.py
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections

RUN apt update && apt install -y git ttf-mscorefonts-installer
RUN git clone --depth 1 https://github.com/tensorflow/models

WORKDIR /tf/github/models/research
RUN /usr/local/bin/protoc object_detection/protos/*.proto --python_out=.

RUN cp object_detection/packages/tf2/setup.py .
RUN python -m pip install .
RUN python -m pip install tensorflowjs

WORKDIR /tf/github/models/research

# インストール成否確認(Confirmation of successful installation)
RUN python object_detection/builders/model_builder_tf2_test.py